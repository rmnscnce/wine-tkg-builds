#!/bin/bash

# preparation

mkdir -p ${HOME}/.config/frogminer/
cp -av remix/wine-tkg.cfg ${HOME}/.config/frogminer

git clone --depth 1 https://github.com/Frogging-Family/wine-tkg-git.git build/

cp -av remix/customization.cfg build/wine-tkg-git/customization.cfg

mkdir -p results/

# build time

pushd build/wine-tkg-git

chmod +x ./non-makepkg-build.sh
./non-makepkg-build.sh
cp -av non-makepkg-builds/* ../../results/

popd

# package as tarballs with XZ

pushd results/

env XZ_OPT='--x86 -8' tar -cJvf wine-tkg.tar.xz ./*

popd
